import { Navigate } from 'react-router-dom';

const RedirectTodo = () => <Navigate to="/AdviceGenerator" />;

export default RedirectTodo;
