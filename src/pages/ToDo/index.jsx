/* eslint-disable react/button-has-type */
/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useEffect, useRef, useState } from 'react';
import Moon from '@static/images/icon-moon.svg';
import Sun from '@static/images/icon-sun.svg';
import { selectListTodo } from '@containers/ToDo/selectors';
import PropTypes from 'prop-types';
import { connect, useDispatch } from 'react-redux';
import { addToDo, toggleToDo, clearCompleted, switchTheme, removeToDo, editTodo } from '@containers/ToDo/actions';
import { createStructuredSelector } from 'reselect';
import { TOGGLE_TODO } from '@containers/ToDo/constants';

import { Delete, Edit } from '@mui/icons-material';
import classes from './style.module.scss';

const ToDo = ({ listToDo }) => {
  // useEffect(() => {
  //   console.log(listToDo);
  // }, [listToDo]);
  const inputRef = useRef(null);
  const [showCompleted, setShowCompleted] = useState(false);
  const dispatch = useDispatch();
  const [isDarkMode, setIsDarkMode] = useState(false);
  const [todos, setTodos] = useState([]);
  const [filter, setFilter] = useState('all');
  // const [editText, setEditText] = useState(listToDo.text);
  const [editedTexts, setEditedTexts] = useState({});
  // const [isEditing, setIsEditing] = useState(false);
  const handleThemeSwitch = () => {
    dispatch(switchTheme());
    setIsDarkMode(!isDarkMode);
  };
  const handleAddTodo = (e) => {
    e.preventDefault();
    const newId = Math.random().toString(36).substring(2, 7);

    const newTodo = { id: newId, text: inputRef.current.value, completed: false };
    if (newTodo.text.trim() !== '') {
      setTodos([...todos, newTodo]);
      inputRef.current.value = '';
      dispatch(addToDo(newTodo));
    }
  };
  const handleClearCompleted = () => {
    dispatch(clearCompleted());
  };

  const handleToggleToDo = (id) => {
    dispatch(toggleToDo(id));
  };

  const handleDeleteTodo = (id) => {
    dispatch(removeToDo(id));
  };

  const handleEditTodo = (id) => {
    if (editedTexts[id] !== undefined) {
      // If the text is already being edited, save the changes
      dispatch(editTodo(id, editedTexts[id]));
      setEditedTexts((prevState) => {
        const newState = { ...prevState };
        delete newState[id]; // Remove the id from editedTexts after saving the changes
        return newState;
      });
    } else {
      const todoToEdit = listToDo.find((todo) => todo.id === id);
      if (todoToEdit) {
        setEditedTexts((prevState) => ({
          // ...prevState,
          [id]: todoToEdit.text,
        }));
      }
      inputRef.current.focus();
    }
  };

  const filteredTodos = listToDo.filter((todo) => {
    if (filter === 'active') {
      return !todo.completed;
    }
    if (filter === 'completed') {
      return todo.completed;
    }
    return true;
  });
  const handleFilterChange = (type) => {
    setFilter(type);
  };

  return (
    <div
      className={`${classes.container} ${isDarkMode ? classes.dark : ''}`}
      style={{ color: isDarkMode ? 'black' : 'white', backgroundColor: isDarkMode ? 'white' : 'hsl(237, 14%, 26%)' }}
    >
      <div className={`${classes.bglight} ${isDarkMode ? classes.dark : ''}`}>
        <div className={classes.title}>
          <h1>TO DO</h1>
          <img
            className={`${classes.moon} ${isDarkMode ? classes.dark : ''}`}
            src={isDarkMode ? Moon : Sun}
            alt="theme-switch-icon"
            onClick={handleThemeSwitch}
          />
        </div>
        <div className={classes.inputContainer}>
          <div className={classes.circle} />
          <form onSubmit={handleAddTodo}>
            <input
              className={`${classes.input} ${isDarkMode ? classes.dark : ''}`}
              style={{
                background: isDarkMode ? 'hsl(236, 33%, 92%)' : 'hsl(233, 14%, 35%)',
                color: isDarkMode ? 'black' : 'white',
              }}
              type="text"
              placeholder="Create a new todo..."
              ref={inputRef}
            />
            <button className={classes.button} type="submit" hidden>
              Add ToDo
            </button>
          </form>
        </div>
        <div
          className={`${classes.todos_container}  ${isDarkMode ? classes.dark : ''}`}
          style={{ background: isDarkMode ? 'hsl(236, 33%, 92%)' : 'hsl(233, 14%, 35%)' }}
        >
          <div className={classes.list} style={{ height: '280px', overflowY: 'scroll', padding: '10px' }}>
            <ul className={classes.todos_list}>
              {filteredTodos.map((todo, index) => {
                const isCompleted = todo.completed;
                if (showCompleted && !isCompleted) {
                  return null; // Skip rendering if the filter is enabled and the todo is not completed
                }
                return (
                  <li key={index} className={classes.todo_item}>
                    <input
                      type="checkbox"
                      className="custom-checkbox"
                      id="myCheckbox"
                      checked={isCompleted}
                      onClick={() => handleToggleToDo(todo.id)}
                    />
                    {editedTexts[todo.id] !== undefined ? (
                      <input
                        className={`${classes.editText}${isDarkMode ? classes.dark : ''}`}
                        style={{
                          background: isDarkMode ? 'white' : 'hsl(233, 14%, 35%)',
                          color: isDarkMode ? 'black' : 'white',
                          border: 'none',
                          width: '200vw',
                        }}
                        type="text"
                        value={editedTexts[todo.id]} // Use editedText as the input field's value
                        onChange={(e) => setEditedTexts({ ...editedTexts, [todo.id]: e.target.value })} // Update the editedText state
                        ref={inputRef}
                      />
                    ) : (
                      <span
                        className={todo.completed ? classes.strikethrough : ''}
                        onClick={() => setEditedTexts({ ...editedTexts, [todo.id]: todo.text })} // Start editing for the specific id
                      >
                        {todo.text}
                      </span>
                    )}

                    <button className={classes.btndelete} onClick={() => handleDeleteTodo(todo.id)} hidden>
                      <Delete style={{ color: 'rgb(194, 59, 77)' }} />
                    </button>
                    <button type="submit" className={classes.btnedit} onClick={() => handleEditTodo(todo.id)}>
                      <Edit style={{ width: '20px', height: '15px', color: 'blue' }} />
                    </button>
                  </li>
                );
              })}
            </ul>
          </div>
          <div className={classes.types} style={{ color: 'gray' }}>
            <p className={classes.items}>{filteredTodos.length} Items left</p>
            <div className={classes.tengah}>
              <p
                className={`${filter === 'all' ? classes.clear : ''} ${classes.custom}`}
                onClick={() => handleFilterChange('all')}
              >
                All
              </p>
              <p
                className={`${filter === 'active' ? classes.clear : ''} ${classes.customActive}`}
                onClick={() => handleFilterChange('active')}
              >
                Active
              </p>
              <p
                className={`${filter === 'completed' ? classes.clear : ''} ${classes.customCompleted}`}
                onClick={() => handleFilterChange('completed')}
              >
                Completed
              </p>
            </div>
            <p className={classes.clearCompleted} onClick={handleClearCompleted}>
              Clear Completed
            </p>
          </div>
          <div
            className={`${classes.types_mobile}   ${isDarkMode ? classes.dark : ''}`}
            style={{
              background: isDarkMode ? 'white' : 'hsl(233, 14%, 35%)',
              height: '7vh',
              color: isDarkMode ? 'hsl(234, 39%, 85%)' : 'hsl(233, 11%, 84%)',
            }}
          >
            {/* <p className={classes.items_mobile}>{filteredTodos.length} Items left</p> */}
            <div className={classes.tengah_mobile}>
              <p
                className={`${filter === 'all' ? classes.clear : ''} ${classes.custom}`}
                onClick={() => handleFilterChange('all')}
              >
                All
              </p>
              <p
                className={`${filter === 'active' ? classes.clear : ''} ${classes.custom}`}
                onClick={() => handleFilterChange('active')}
              >
                Active
              </p>
              <p
                className={`${filter === 'completed' ? classes.clear : ''} ${classes.custom}`}
                onClick={() => handleFilterChange('completed')}
              >
                Completed
              </p>
            </div>
            {/* <p className={classes.clearCompleted} onClick={handleClearCompleted}>
              Clear Completed
            </p> */}
          </div>
        </div>
        <div className={classes.text_bottom}>
          <p>Drag and drop to reorder list</p>
        </div>
      </div>
    </div>
  );
};
ToDo.propTypes = {
  listToDo: PropTypes.array,
};
const mapStateToProps = createStructuredSelector({
  listToDo: selectListTodo,
});
const mapDispatchToProps = {
  handleClearCompleted: clearCompleted,
};
export default connect(mapStateToProps, mapDispatchToProps)(ToDo);
