/* eslint-disable react/no-unknown-property */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/button-has-type */
import { useEffect, useState } from 'react';
import devider from '@static/images/pattern-divider-desktop.png';
import { getAdvice, switchTheme } from '@containers/App/actions';
import { selectIsDarkMode, selectAdviceLoading } from '@containers/App/selectors';
import PropTypes from 'prop-types';

import { createStructuredSelector } from 'reselect';
import { useDispatch, useSelector, connect } from 'react-redux';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import LinearProgress from '@mui/material/LinearProgress';
import CircularProgress from '@mui/material/CircularProgress';
import AcUnitIcon from '@mui/icons-material/AcUnit';
import LocalFireDepartmentIcon from '@mui/icons-material/LocalFireDepartment';
// import NightsStayIcon from '@mui/icons-material/NightsStay';
// import WbSunnyIcon from '@mui/icons-material/WbSunny';
import { IconButton } from '@mui/material';
import dice from '@static/images/icon-dice.png';
import classes from './style.module.scss';

const AdviceGenerator = ({ theme, adviceLoading }) => {
  const [rotate, setRotate] = useState(false);
  const dispatch = useDispatch();

  const isLight = theme === 'dark';

  const handleTheme = () => {
    if (theme === 'dark') {
      dispatch(switchTheme('light'));
      document.body.classList.remove('dark-theme');
      localStorage.setItem('theme', 'light'); // Store the theme in local storage
    } else {
      dispatch(switchTheme('dark'));
      document.body.classList.add('dark-theme');
      localStorage.setItem('theme', 'dark'); // Store the theme in local storage
    }
  };

  useEffect(() => {
    const storedTheme = localStorage.getItem('theme');
    if (storedTheme && storedTheme === 'dark') {
      dispatch(switchTheme('dark'));
      document.body.classList.add('dark-theme');
    } else {
      dispatch(switchTheme('light'));
      document.body.classList.remove('dark-theme');
    }
    // Dispatch the getAdvice action on component mount
    dispatch(getAdvice());
  }, [dispatch]);

  const myTheme = createTheme({
    palette: {
      text: {
        primary: isLight ? 'hsl(241, 19%, 90%)' : 'hsl(200, 15%, 8%)',
      },
    },
  });

  const adviceData = useSelector((state) => state.app.generalAdvice);
  return (
    <ThemeProvider theme={myTheme}>
      <div className={classes.rightHeader} variant="contained" onClick={handleTheme}>
        <IconButton>
          {theme === 'light' ? (
            <AcUnitIcon sx={{ fontSize: 36, color: 'white', '&:hover': { color: 'blue' } }} />
          ) : (
            <LocalFireDepartmentIcon sx={{ fontSize: 36, color: 'white', '&:hover': { color: 'red' } }} />
          )}
        </IconButton>
      </div>
      <div className={isLight ? classes.App : classes.AppDark}>
        <div className={isLight ? classes.content : classes.contentRandom}>
          <h4 className={isLight ? classes.head : classes.headRandom}>
            Advice #
            {adviceLoading === true ? (
              <LinearProgress
                sx={{
                  background: 'hsl(150, 100%, 66%)',
                  width: '80px',
                  margin: '0 auto',
                }}
              />
            ) : (
              adviceData?.id
            )}
          </h4>
          <div className={isLight ? classes.advice : classes.adviceRandom}>
            <p>
              “
              {adviceLoading === true ? (
                <CircularProgress
                  sx={{
                    color: 'success',
                    width: '80px',
                    margin: '0 auto',
                  }}
                />
              ) : (
                adviceData?.advice
              )}
              ”
            </p>
          </div>
          <div className={classes.bottom}>
            <img src={devider} />
            <button
              className={isLight ? classes.btn : classes.btnRandom}
              onClick={() => {
                window.location.reload();
              }}
            >
              <img
                className={`${classes.dice} ${rotate ? classes.rotate : ''}`}
                src={dice}
                onClick={() => {
                  setRotate(true);
                }}
                alt="Dice"
              />
            </button>
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
};

AdviceGenerator.propTypes = {
  theme: PropTypes.string,

  adviceLoading: PropTypes.bool,
};
const mapStateToProps = createStructuredSelector({
  theme: selectIsDarkMode,
  adviceLoading: selectAdviceLoading,
});
export default connect(mapStateToProps)(AdviceGenerator);
