import MainLayout from '@layouts/MainLayout';

// import Home from '@pages/Home';
// import ToDo from '@pages/ToDo';
import NotFound from '@pages/NotFound';
// import RedirectTodo from '@components/RedirectTodo';
import AdviceGenerator from '@pages/AdviceGenerator';

const routes = [
  {
    path: '/',
    name: 'AdviceGenerator',
    // component: RedirectTodo,
    component: AdviceGenerator,
    // layout: MainLayout,
  },
  // {
  //   path: '/advice',
  //   name: 'AdviceGenerator',
  //   component: AdviceGenerator,
  //   // layout: MainLayout,
  // },

  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout },
];

export default routes;
