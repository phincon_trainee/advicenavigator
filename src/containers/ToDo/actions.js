import { ADD_TODO, REMOVE_TODO, TOGGLE_TODO, CLEAR_COMPLETED, SWITCH_THEME, EDIT_TODO } from './constants';

export const addToDo = (listToDo) => ({
  type: ADD_TODO,
  listToDo,
});

export const removeToDo = (id) => ({
  type: REMOVE_TODO,
  id,
});

export const toggleToDo = (id) => ({
  type: TOGGLE_TODO,
  id,
});

export const clearCompleted = () => ({
  type: CLEAR_COMPLETED,
});

export const switchTheme = (id) => ({
  type: SWITCH_THEME,
  id,
});

export const editTodo = (id, newText) => ({
  type: EDIT_TODO,
  payload: {
    id,
    newText,
  },
});
