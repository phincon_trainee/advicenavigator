import { takeLatest, call, put } from 'redux-saga/effects';

import { GET_ADVICES } from '@containers/App/constants';
import { setAdvice, setAdviceLoading } from '@containers/App/actions';

import { getAdvice } from '@domain/api';

export function* doGetGeneralAdvice() {
  yield put(setAdviceLoading(true));
  try {
    const generalAdvice = yield call(getAdvice);
    // console.log(generalAdvice);
    if (generalAdvice) {
      yield put(setAdvice(generalAdvice.slip));
    }
  } catch (error) {
    // yield put(setError.getAdviceFailure(error));
    // yield put(setError.setError(error.message));
    // eslint-disable-next-line no-console
    console.log(error);
  }
  yield put(setAdviceLoading(false));
}
export default function* appSaga() {
  yield takeLatest(GET_ADVICES, doGetGeneralAdvice);
}
