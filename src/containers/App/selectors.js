import { createSelector } from 'reselect';
import { initialState } from '@containers/App/reducer';

const selectAppState = (state) => state.app || initialState;

const selectGreetings = createSelector(selectAppState, (state) => state.greetings);
const selectCountryList = createSelector(selectAppState, (state) => state.countryList);
const selectGeneralAdvice = createSelector(selectAppState, (state) => state.advice);

export const selectIsDarkMode = createSelector([selectAppState], (state) => state.theme);
export const selectAdviceLoading = createSelector(selectAppState, (state) => state.adviceLoading);
// export const selectAdviceError = createSelector(selectAppState, (state) => state.error);

export { selectGreetings, selectCountryList, selectGeneralAdvice };
