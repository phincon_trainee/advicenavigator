import {
  SET_GREETINGS,
  GET_COUNTRY_LIST,
  SET_COUNTRY_LIST,
  GET_ADVICES,
  SET_ADVICES,
  SWITCH_THEME,
  SET_ADVICE_LOADING,
  // SET_ERROR,
} from '@containers/App/constants';

export const setGreetings = (greetings) => ({
  type: SET_GREETINGS,
  greetings,
});

export const getCountryList = () => ({
  type: GET_COUNTRY_LIST,
});

export const setCountryList = (countryList) => ({
  type: SET_COUNTRY_LIST,
  countryList,
});

export const getAdvice = () => ({
  type: GET_ADVICES,
});

export const setAdvice = (advice) => ({
  type: SET_ADVICES,
  advice,
});

export const switchTheme = (theme) => ({
  type: SWITCH_THEME,
  theme,
});
export const setAdviceLoading = (adviceLoading) => ({
  type: SET_ADVICE_LOADING,
  adviceLoading,
});

// export const setError = (error) => ({
//   type: SET_ERROR,
//   error,
// });
