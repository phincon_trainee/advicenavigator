import { produce } from 'immer';

import {
  SET_GREETINGS,
  SET_COUNTRY_LIST,
  SET_ADVICES,
  SWITCH_THEME,
  SET_ADVICE_LOADING,
  // SET_ERROR,
} from '@containers/App/constants';

export const initialState = {
  greetings: 'Hi from web!',
  countryList: [],
  generalAdvice: {},
  theme: 'light',
  adviceLoading: false,
  // error: null,
};

// eslint-disable-next-line default-param-last
const appReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_GREETINGS:
        draft.greetings = action.greetings;
        break;
      case SET_COUNTRY_LIST:
        draft.countryList = action.countryList;
        break;
      case SET_ADVICES:
        draft.generalAdvice = action.advice;
        break;
      case SWITCH_THEME:
        draft.theme = action.theme;
        break;
      case SET_ADVICE_LOADING:
        draft.adviceLoading = action.adviceLoading;
        break;
      // case SET_ERROR:
      //   draft.error = action.error; // Update the error state
      //   break;
      default:
        break;
    }
  });

export default appReducer;
